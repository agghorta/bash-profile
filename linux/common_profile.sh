#Linux General profile settings

#----------
# Functions:
#----------

function showUpdates() {
    if [ "$1" == "list" ]
    then
    	/usr/lib/update-notifier/apt-check -p;
    else
    	/usr/lib/update-notifier/apt-check --human-readable
    fi
 }

#----------
# Aliases:
#----------
alias ls='ls -Fh --color=auto'
alias ll="ls -alhF --group-directories-first"
alias la="ls -A --group-directories-first"
alias l='ls -CF --group-directories-first'
alias firewall='sudo ufw'
alias updateMyDateTime="sudo ntpdate time.nist.gov"
alias ubuntuVersion="lsb_release -a"
alias version="lsb_release -a"
alias glances="/usr/local/bin/glances"
alias pull="git pull"
alias p="git pull"

#-----------------
# Terminal Colors
#-----------------
LS_COLORS="$LS_COLORS:di=0;96:"
export LS_COLORS
alias ls='ls -Fh --color=auto'