#------------------
#Bash Profile
#Version: 3.9.1
#------------------

#--------------
# Startup
#--------------

export EDITOR="vi"

if [[ ! -x $(which fink) && -d /sw/bin ]];then 
        source /sw/bin/init.sh
fi

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

if [ -f ~/.git-completion.bash ]; then # not me, something put it here
    . ~/.git-completion.bash
fi

export PATH=/usr/local/bin:/opt/local/bin:/opt/local/sbin:$PATH

HISTCONTROL=ignoreboth # ignore spaces and duplicates from command history
HISTSIZE=1500 # size of history file
HISTFILE=${ZDOTDIR:-$HOME}/.zsh_history # Set a file for history

setopt AUTO_CD
# share history across multiple zsh sessions
setopt SHARE_HISTORY
# append to history
setopt APPEND_HISTORY
# adds commands as they are typed, not at shell exit
setopt INC_APPEND_HISTORY
# expire duplicates first
setopt HIST_EXPIRE_DUPS_FIRST 
# do not store duplications
setopt HIST_IGNORE_DUPS
#ignore duplicates when searching
setopt HIST_FIND_NO_DUPS
# removes blank lines from history
setopt HIST_REDUCE_BLANKS
#autocorrect
setopt CORRECT
setopt CORRECT_ALL

#------------------
# Custom Functions
#------------------

# ff:  to find a file under the current directory
ff () { /usr/bin/find . -name "$@"; }
# ffs: to find a file whose name starts with a given string
ffs () { /usr/bin/find . -name "$@"'*'; }
# ffe: to find a file whose name ends with a given string
ffe () { /usr/bin/find . -name '*'"$@"; }
# findlarger: to find a file Larger than a set size
findlarger() { find . -type f -size +${1}c; }


# Create a new directory and enter it
function md() {
        mkdir -p "$@" && cd "$@"
}

# Custom grep wrapper
g() {
    grep -HRins -C3 --colour=always "$@" .;
}

# Display a text with borders to console
function sayText() {
                echo ""
                echo ""
        echo "****************************"
        echo "*"
        echo "* $1"
        echo "*"
        echo "****************************"
                echo ""
}

# Search and destroy, searches text in file, then replaces first arg with second arg
sad() { grep -rls $1 . | grep -v svn | xargs sed -i -e "s/$1/$2/"; }

profileversion() {
  echo "
#Version: 3.9.1
    "
}

updatebashprofile() { #broken
  force=$1;
  pwd=$(pwd);
  cd ~;
  if [ "$force" == "force" ]; then
    echo "updating bash profile";
    wget --no-cache -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/prod/install/install.sh && source ~/install.sh && rm ~/install.sh;
  else
    wget --no-cache --quiet -O ~/bversion.txt "https://bitbucket.org/agghorta/bash-profile/raw/prod/version.txt";
    newVersion=$(<bversion.txt);
    currentVersion="$(profileversion | xargs)";
    if [ "$newVersion" == "$currentVersion" ]; then
      echo "bash profile is up to date";
      rm bversion.txt;
    else
      echo "updating bash profile";
      rm bversion.txt;
      wget --no-cache -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/prod/install/install.sh && source ~/install.sh && rm ~/install.sh;
    fi
  fi
  cd $pwd;
}

# Use Git’s colored diff when available
hash git &>/dev/null
if [ $? -eq 0 ]; then
        function diff() {
                git diff --no-index --color-words "$@"
        }
fi

#run php lint before git add
function git_add() { php -l $1 && git add $1; }

function gitCheckAdd() {
  file=$1;
  git diff $1;
  echo "Add to Git?"
  select addToGit in "yes" "no"; do
    if [ "$addToGit" == "no" ]; then
      break;
    else
      git_add $1;
      break;
    fi
  done
}

#cleanup local git branchs
function cleanupbranches() {
  git checkout master
  git fetch -p
  git remote prune origin
  git branch --merged master | grep -v 'master$' | xargs -n 1 git branch -d
}

#automate git rebase to do a git fetch
function git_rebase() { git fetch && git rebase -i origin/master; }

#print the current git branch
function print_git_branch() { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* //'; }

#print the left square brace if there is a git branch
function sb1() {
  A=`print_git_branch`
  B=""
  if [ "$A" == "$B" ]
  then
    echo "";
  else
    echo "[";
  fi
}

#print the right square brace if there is a git branch
function sb2() {
  A=`print_git_branch`
  B=""
  if [ "$A" == "$B" ]
  then
    echo "";
  else
    echo "]";
  fi
}

#-----------------------
# Custom Command Prompt
#-----------------------
WHITE="white"
LIGHT_WHITE="255"
BLACK="black"
GRAY="8"
DARK_GRAY="240"
SILVER="7"
RED="196"
LIGHT_RED="red"
GREEN="46"
LIGHT_GREEN="green"
YELLOW="226"
LIGHT_YELLOW="yellow"
BLUE="69"
LIGHT_BLUE="blue"
MAGENTA="164"
LIGHT_MAGENTA="magenta"
CYAN="159"
LIGHT_CYAN="cyan"
ORANGE="166"

#git
RPROMPT_PREFIX='%{'$'\e[1A''%}' # one line up
RPROMPT_SUFFIX='%{'$'\e[1B''%}' # one line down

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT="$RPROMPT_PREFIX"\$vcs_info_msg_0_"$RPROMPT_SUFFIX"   #hack para ter o rprompt na primeira linha
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' formats '%F{$LIGHT_GREEN}<%b>%u%c%f'
zstyle ':vcs_info:git:*' stagedstr ' (*)'
zstyle ':vcs_info:git:*' unstagedstr ' (!)'


#GENERAL
BASH_COLOR=$YELLOW
SERVER_NAME_COLOR=$YELLOW
SERVER_NAME="MAC HOME"
INFO_TEXT_COLOR=$LIGHT_WHITE
GIT_COLOR=$LIGHT_GREEN

PROMPT="%F{$BASH_COLOR}┌─%F{$INFO_TEXT_COLOR}${SERVER_NAME}%F{$BASH_COLOR}───[%F{$INFO_TEXT_COLOR}%*%F{$BASH_COLOR}][%B%F{$INFO_TEXT_COLOR}%~%b%F{$BASH_COLOR}][%F{$INFO_TEXT_COLOR}%h%F{$BASH_COLOR}][%(?.%F{green}√.%F{red}X)%F{$BASH_COLOR}]%f
%F{$BASH_COLOR}└──▪ %f"


PROMPT="%F{226}┌─%F{white}TEST%F{226}───[%F{white}%*%F{226}][%F{white}%~%F{226}][%F{white}%h%F{226}]%?%f"$'\n'"%F{226}└──▪ %f"

PROMPT="%F${BASH_COLOR}┌─%F${SERVER_NAME_COLOR}${SERVER_NAME}${BASH_COLOR}───[${INFO_TEXT_COLOR}\t${NO_COLOR}${BASH_COLOR}][${INFO_TEXT_COLOR}\w${NO_COLOR}${BASH_COLOR}]\$(sb1)${NO_COLOR}${GIT_COLOR}\$(print_git_branch)${NO_COLOR}${BASH_COLOR}\$(sb2)[${NO_COLOR}${INFO_TEXT_COLOR}!\!${NO_COLOR}${BASH_COLOR}]${NO_COLOR}
${BASH_COLOR}└──▪${NO_COLOR} "

export GREP_COLOR='1;33'

#----------
# Aliases:
#----------
alias myip="ifconfig"
alias help="man"
alias mysqladmin="/usr/local/mysql/bin/mysqladmin"
alias grep="grep -Hins -C3 --colour=always"
alias fgrep='fgrep -Hins -C3 --color=always'
alias egrep='egrep -Hins -C3 --color=always'
alias l.='ls -d .* --color=auto'
alias findtext="grep -r -o -i $1 *"
alias please='sudo $(fc -ln -1)'
alias bashversion="profileversion"
alias firewall='sudo ufw'
alias bashupdate="updatebashprofile"
alias bashprofileupdate="updatebashprofile"
alias dirsize="du -chd 0 *"
alias foldersize="du -chd 0 *"


# git aliases
alias gd="git difftool -y --tool=opendiff "
alias gmt="git mergetool -y --tool=opendiff "
alias s="git status "
alias b="git branch "
alias gcom="git commit -am "
alias gc="git commit -m "
alias merge="git merge "
alias stash="git stash "
alias p=pull
alias push="git push "
alias gprm="git pull -r origin master"
alias gpr="git pull -r origin "
alias ga=git_add
alias gca=gitCheckAdd
alias reb=git_rebase
alias c="git checkout "
alias glog="git log --decorate --pretty=short --graph --source "

#Work MAC OS Specific bash profile

#----------
# Functions:
#----------

# git pull
function pull() {
    pwd=$(pwd);
    if [ "$1" == "all" ]
    then
        projects=$(ls -d1 $HOME/projects/*/);
        for project in $projects; do
            sayText "'Getting $(basename $project)";
            git -C $project pull;
        done
        cd "$pwd";
    else
        if [ "$1" == "42" ]
        then
            projects=( "alerts_client" "common_api" "common_client" "infomartcom"
            "infomartcom2" "petrie_api" "petrie_web" )
            pull_project() {
                echo "Getting $1"
                cd $HOME/projects/$1;
                git pull;
                cd "$pwd";
            }

            for folder in "${projects[@]}"
            do
                pull_project $folder;
            done

            cd "$pwd";
        else
            git pull $1 $2 $3 $4;
        fi
    fi

}

# Remove the Mac OS extended Attributes
function removeExtendedAttributes() {
    sudo chflags -R nouchg $1;
    xattr -r -c $1;
}

#----------
# Exports and ETC
#----------

# Pip is using virtualenv. That is forcing pip to use it.
#export PIP_REQUIRE_VIRTUALENV=true
# For pip, On some systems you may need to export a PYTHONPATH variable depending on the tool you're using.
#export PYTHONPATH=$HOME/local/lib/python2.7/site-packages/
# Authentication Key for infomart NPM
export NEXUS_NPM_AUTH=YWRyaWFuOkBkcmlhbmNvZGVzbGFiczEyM3E=

# Set AWS Keys to ssh to aws and work with aws command line
export AWS_ACCESS_KEY="AKIAJWHV3XBIWKEHXT4Q"
export AWS_SECRET_KEY="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"
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY


export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion" ] && . "/usr/local/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion



#----------
# Aliases:
#----------
alias ls='ls -GFh'
alias ll="ls -alhF"
alias la="ls -A"
alias l='ls -CF'
alias sudo='sudo '
alias su='su -'
alias unlockFolder="removeExtendedAttributes"

#-----------------
# Terminal Colors
#-----------------
export CLICOLOR=1
export LSCOLORS=exfxcxdxbxegedabagacad




#----------
# Exports and ETC
#----------

# Configuration for Activator for scala Dev
export PROJECTS_HOME="/Users/andre/projects"
export ACTIVATOR_HOME=$PROJECTS_HOME/activator
export PATH=$PATH:$ACTIVATOR_HOME/bin #Home Mac Specific bash profile
export VAGRANT_HOME="/Volumes/DadosMac/VagrantBoxes"
export B2_ACCOUNT_ID=001420207e42b980000000004
export B2_ACCOUNT_KEY=K00124wxo3qpD3rv8pniOboO6zJeC9M


#---------------------
# Calling ScreenFetch
#---------------------
screenfetch -E
export PATH="/usr/local/opt/openssl/bin:$PATH"

