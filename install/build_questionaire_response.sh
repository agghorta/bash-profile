#questions#################################################################################################################

if [ "$superuser" == "yes" ]; then #---------- if Superuser ----------------
	
	box_name="superuser";
    source $pathToUserBashSettings;
	get_box_configuration;

else #---------- Else Superuser ----------------

	echo "Quick selection and install everything?"
	select QUICKSELECTION in "yes" "no"; do
		break
	done

	if [ "$QUICKSELECTION" == "yes" ]; then #---------- Quick selection ----------------
		
		echo "Select what box you are installing"
		select box_name in "${SERVERS_NAMES[@]}"; do
			for server in "${SERVERS_NAMES[@]}"; do
				if [[ $server == $box_name ]]; then
					break 2
				fi
			done
		done
		get_box_configuration;

	else #---------- Else Quick selection ----------------
		echo "In what OS do you wish to install this bash profile?"
		select OS in "mac" "linux"; do
			break
		done

        echo "In what environment do you wish to install this bash profile?"
		select ENVIRONMENT_INCLUDES in "work" "home"; do
            ENVIRONMENT_INCLUDES=( "$ENVIRONMENT_INCLUDES" );
			break;
		done

        echo "Select the computer name for the prompt"
        if [ "$OS" == "mac" ]; then
            select name in "Host Name" "MAC" "Enter Name" "Do not display computer name"; do
                case $name in 
                    "MAC" ) SERVER_NAME='MAC'; break;;
                    "Host Name" ) SERVER_NAME='\\\\h'; break;;
                    "Do not display computer name" ) SERVER_NAME=''; break;;
                    "Enter Name" ) echo "please enter the computer name:";read SERVER_NAME; break;;
                esac
            done
        else
            select name in "Host Name" "Linux Box" "Enter Name" "Do not display computer name"; do
                case $name in 
                    "Linux Box" ) SERVER_NAME="Linux Box"; break;;
                    "Host Name" ) SERVER_NAME='\\\\h'; break;;
                    "Do not display computer name" ) SERVER_NAME=''; break;;
                    "Enter Name" ) echo "please enter the computer name:";read SERVER_NAME; break;;
                esac
            done
        fi

        echo "Select the bash color"
        select color in  "Black" "Gray" "White" "Light White" "Red" "Light Red" "Green" "Light Green" "Yellow" "Light Yellow" "Blue" "Light Blue" "Magenta" "Light Magenta" "Cyan" "Light Cyan"; do
            case $color in
                "Black" ) BASH_COLOR='$BLACK'; break;;
                "Gray" ) BASH_COLOR='$GRAY'; break;;
                "White" ) BASH_COLOR='$WHITE'; break;;
                "Light White" ) BASH_COLOR='$LIGHT_WHITE'; break;;
                "Red" ) BASH_COLOR='$RED'; break;;
                "Light Red" ) BASH_COLOR='$LIGHT_RED'; break;;
                "Green" ) BASH_COLOR='$GREEN'; break;;
                "Light Green" ) BASH_COLOR='$LIGHT_GREEN'; break;;
                "Yellow" ) BASH_COLOR='$YELLOW'; break;;
                "Light Yellow" ) BASH_COLOR='$LIGHT_YELLOW'; break;;
                "Blue" ) BASH_COLOR='$BLUE'; break;;
                "Light Blue" ) BASH_COLOR='$LIGHT_BLUE'; break;;
                "Magenta" ) BASH_COLOR='$MAGENTA'; break;;
                "Light Magenta" ) BASH_COLOR='$LIGHT_MAGENTA'; break;;
                "Cyan" ) BASH_COLOR='$CYAN'; break;;
                "Light Cyan" ) BASH_COLOR='$LIGHT_CYAN'; break;;
            esac
        done
        echo "Select the server name color"
        select color in  "Black" "Gray" "White" "Light White" "Red" "Light Red" "Green" "Light Green" "Yellow" "Light Yellow" "Blue" "Light Blue" "Magenta" "Light Magenta" "Cyan" "Light Cyan"; do
            case $color in
                "Black" ) SERVER_NAME_COLOR='$BLACK'; break;;
                "Gray" ) SERVER_NAME_COLOR='$GRAY'; break;;
                "White" ) SERVER_NAME_COLOR='$WHITE'; break;;
                "Light White" ) SERVER_NAME_COLOR='$LIGHT_WHITE'; break;;
                "Red" ) SERVER_NAME_COLOR='$RED'; break;;
                "Light Red" ) SERVER_NAME_COLOR='$LIGHT_RED'; break;;
                "Green" ) SERVER_NAME_COLOR='$GREEN'; break;;
                "Light Green" ) SERVER_NAME_COLOR='$LIGHT_GREEN'; break;;
                "Yellow" ) SERVER_NAME_COLOR='$YELLOW'; break;;
                "Light Yellow" ) SERVER_NAME_COLOR='$LIGHT_YELLOW'; break;;
                "Blue" ) SERVER_NAME_COLOR='$BLUE'; break;;
                "Light Blue" ) SERVER_NAME_COLOR='$LIGHT_BLUE'; break;;
                "Magenta" ) SERVER_NAME_COLOR='$MAGENTA'; break;;
                "Light Magenta" ) SERVER_NAME_COLOR='$LIGHT_MAGENTA'; break;;
                "Cyan" ) SERVER_NAME_COLOR='$CYAN'; break;;
                "Light Cyan" ) SERVER_NAME_COLOR='$LIGHT_CYAN'; break;;
            esac
        done
        echo "Select the prompt path text color"
        select color in  "Black" "Gray" "White" "Light White" "Red" "Light Red" "Green" "Light Green" "Yellow" "Light Yellow" "Blue" "Light Blue" "Magenta" "Light Magenta" "Cyan" "Light Cyan"; do
            case $color in
                "Black" ) INFO_TEXT_COLOR='$BLACK'; break;;
                "Gray" ) INFO_TEXT_COLOR='$GRAY'; break;;
                "White" ) INFO_TEXT_COLOR='$WHITE'; break;;
                "Light White" ) INFO_TEXT_COLOR='$LIGHT_WHITE'; break;;
                "Red" ) INFO_TEXT_COLOR='$RED'; break;;
                "Light Red" ) INFO_TEXT_COLOR='$LIGHT_RED'; break;;
                "Green" ) INFO_TEXT_COLOR='$GREEN'; break;;
                "Light Green" ) INFO_TEXT_COLOR='$LIGHT_GREEN'; break;;
                "Yellow" ) INFO_TEXT_COLOR='$YELLOW'; break;;
                "Light Yellow" ) INFO_TEXT_COLOR='$LIGHT_YELLOW'; break;;
                "Blue" ) INFO_TEXT_COLOR='$BLUE'; break;;
                "Light Blue" ) INFO_TEXT_COLOR='$LIGHT_BLUE'; break;;
                "Magenta" ) INFO_TEXT_COLOR='$MAGENTA'; break;;
                "Light Magenta" ) INFO_TEXT_COLOR='$LIGHT_MAGENTA'; break;;
                "Cyan" ) INFO_TEXT_COLOR='$CYAN'; break;;
                "Light Cyan" ) INFO_TEXT_COLOR='$LIGHT_CYAN'; break;;
            esac
        done
        echo "Select the git branch name color"
        select color in  "Black" "Gray" "White" "Light White" "Red" "Light Red" "Green" "Light Green" "Yellow" "Light Yellow" "Blue" "Light Blue" "Magenta" "Light Magenta" "Cyan" "Light Cyan"; do
            case $color in
                "Black" ) GIT_COLOR='$BLACK'; break;;
                "Gray" ) GIT_COLOR='$GRAY'; break;;
                "White" ) GIT_COLOR='$WHITE'; break;;
                "Light White" ) GIT_COLOR='$LIGHT_WHITE'; break;;
                "Red" ) GIT_COLOR='$RED'; break;;
                "Light Red" ) GIT_COLOR='$LIGHT_RED'; break;;
                "Green" ) GIT_COLOR='$GREEN'; break;;
                "Light Green" ) GIT_COLOR='$LIGHT_GREEN'; break;;
                "Yellow" ) GIT_COLOR='$YELLOW'; break;;
                "Light Yellow" ) GIT_COLOR='$LIGHT_YELLOW'; break;;
                "Blue" ) GIT_COLOR='$BLUE'; break;;
                "Light Blue" ) GIT_COLOR='$LIGHT_BLUE'; break;;
                "Magenta" ) GIT_COLOR='$MAGENTA'; break;;
                "Light Magenta" ) GIT_COLOR='$LIGHT_MAGENTA'; break;;
                "Cyan" ) GIT_COLOR='$CYAN'; break;;
                "Light Cyan" ) GIT_COLOR='$LIGHT_CYAN'; break;;
            esac
        done

        echo "Do you wish to install screenfetch?"
        select SCREENFETCH in "yes" "no"; do
            break
        done

        if [ "$computerType" != "MAC Computer" ]; then
            echo "Do you wish to install VIM colors?"
            select VIMCOLORS in "yes" "no"; do
                break
            done
        else    
            VIMCOLORS = "no";
        fi

        

        #get_env
        
	fi #---------- End Quick selection ----------------
fi #---------- End Superuser ----------------