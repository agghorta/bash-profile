# Documentation

# There are 3 main groups at the moment. Mac computer, Home Linux and Work Linux

# To add a new profile configuration you need to add a new file under the settings folder that has a name 
# using the following convention: {machine_name}_settings.sh. The machine_name will be used to list the machine name on the machine
# selection during the script execution.
# inside the setting you need to add the following configuration:


	# #General Settings
	# screenfetch="yes|no" -> Install screenfetch on the machine
	# vimcolors="yes|no" -> add font colors to VIM when working with code 
	# computerType="Work Linux Box|MAC Computer"
	# SERVER_NAME="VM"

	# #Color settings
	# BASH_COLOR='MAGENTA'
	# SERVER_NAME_COLOR='GREEN'
	# INFO_TEXT_COLOR='LIGHT_WHITE'
	# GIT_COLOR='LIGHT_GREEN'


#Setting parameters for the superuser run
superuser=$1;
if [ "$superuser" == "" ]; then
	superuser="no";
fi
pathToUserBashSettings=$2;

echo superuser $superuser;
echo path $pathToUserBashSettings;


#bootstrap and support functions#################################################################################################################
WorkPath=$HOME"/bashProfileInstall/";
branch="master";
vimcolors="no";
screenfetch="no";


#creatig the work directory
if [ ! -d $WorkPath ]; then
    mkdir $WorkPath;
fi

function sayText() {
		echo ""
		echo ""
        echo "****************************"
        echo "*"
        echo "* $1"
        echo "*"
        echo "****************************"
		echo ""
}

#Insert a file to bash profile
function addToBashProfile() {
    a=$WorkPath"temporario"
    sayText "Adding $1 to the bash profile"
    wget --no-cache -O ${WorkPath}temporario $2
   
    sed -i -e "\#$1#{
        r $a
        d
    }" ${WorkPath}bash_profile_temp
}

function get_file() {
	wget --no-cache -O $WorkPath$1 $2	
}

function get_box_configuration() {
	get_file bash_settings "https://bitbucket.org/agghorta/bash-profile/raw/$branch/Settings/"$box_name"_settings.sh"
	source ${WorkPath}bash_settings
}

#Getting the server Names
get_file server_names.sh https://bitbucket.org/agghorta/bash-profile/raw/$branch/install/server_names.sh
. ${WorkPath}server_names.sh


#installation script#################################################################################################################

#Run all questions before moving forward
get_file build_questionaire_response.sh https://bitbucket.org/agghorta/bash-profile/raw/$branch/install/build_questionaire_response.sh
. ${WorkPath}build_questionaire_response.sh


##############  get the initial bash profile template ##############
sayText "Getting the bash profile template"
get_file bash_profile_temp https://bitbucket.org/agghorta/bash-profile/raw/$branch/install/bash_profile_template.sh


############## Get the color template ##############
get_file env_colors https://bitbucket.org/agghorta/bash-profile/raw/$branch/colors/default_colors.sh

#Transform the responses into variable to transalate to the color
BASH_COLOR="$"${BASH_COLOR}
SERVER_NAME_COLOR="$"${SERVER_NAME_COLOR}
INFO_TEXT_COLOR="$"${INFO_TEXT_COLOR}
GIT_COLOR="$"${GIT_COLOR}

# #Applying color to color file
a='CUSTOM_BASH_COLOR'
sed -i -e "s/$a/$BASH_COLOR/g" ${WorkPath}env_colors
a='CUSTOM_SERVER_NAME_COLOR'
sed -i -e "s/$a/$SERVER_NAME_COLOR/g" ${WorkPath}env_colors
a='CUSTOM_INFO_TEXT_COLOR'
sed -i -e "s/$a/$INFO_TEXT_COLOR/g" ${WorkPath}env_colors
a='CUSTOM_SERVER_NAME'
sed -i -e "s/$a/$SERVER_NAME/g" ${WorkPath}env_colors
a='CUSTOM_GIT_COLOR'
sed -i -e "s/$a/$GIT_COLOR/g" ${WorkPath}env_colors


#Insert Environment Colors
a=$WorkPath"env_colors"
sayText "Adding $a to the bash profile"

ENVIRONMENT_NAME=${ENVIRONMENT_INCLUDES[1]}
if [ "$OS" == "mac" ]; then
		get_file custom_colors https://bitbucket.org/agghorta/bash-profile/raw/$branch/colors/custom_colors_mac.sh
else
	get_file custom_colors https://bitbucket.org/agghorta/bash-profile/raw/$branch/colors/custom_colors.sh
fi
sed -i -e "\#COLOR_SETTINGS_PLACE_HOLDER#{
r $a
d
}" ${WorkPath}custom_colors

a=${WorkPath}"custom_colors"
sayText "Adding $a to the bash profile"
sed -i -e "\#CUSTOM_COLORS#{
r $a
d
}" ${WorkPath}bash_profile_temp

#Insert bootsrap to bash profile
addToBashProfile "BOOTSTRAP" "https://bitbucket.org/agghorta/bash-profile/raw/$branch/common/bootstrap.sh"

#Insert functions to bash profile
addToBashProfile "FUNCTIONS" "https://bitbucket.org/agghorta/bash-profile/raw/$branch/common/functions.sh"

#Insert aliases to bash profile
addToBashProfile "ALIASES" "https://bitbucket.org/agghorta/bash-profile/raw/$branch/common/aliases.sh"

#Adding bash profile version
addToBashProfile "BASH_PROFILE_VERSION" "https://bitbucket.org/agghorta/bash-profile/raw/$branch/version.txt"

#Insert custom to OS code to bash profile
for ENVIRONMENT in "${ENVIRONMENT_INCLUDES[@]}"; do
	get_file temporario "https://bitbucket.org/agghorta/bash-profile/raw/$branch/$OS/${ENVIRONMENT}_profile.sh";
	cat ${WorkPath}temporario >> ${WorkPath}full_environment;
	echo "" >> ${WorkPath}full_environment;
	echo "" >> ${WorkPath}full_environment;
	echo "" >> ${WorkPath}full_environment;
	echo "" >> ${WorkPath}full_environment;
done

a=$WorkPath"full_environment"
sayText "Adding OS_SPECIFIC to the bash profile"   
sed -i -e "\#OS_SPECIFIC#{
	r $a
	d
}" ${WorkPath}bash_profile_temp

if [ "$superuser" != "yes" ]; then
	if [ "$OS" == "linux" ]; then
		sudo apt-get update
		sudo apt-get -y install elinks python-pip python-dev build-essential 
		sudo pip install --upgrade pip 
		sudo pip install --upgrade virtualenv 
		sudo pip install --upgrade glances 
	else
		export PIP_REQUIRE_VIRTUALENV=false
		sudo easy_install pip
		sudo chown -R $USER /usr/local 
		brew update
		pip install --upgrade pip setuptools
		
		#Install/update wget
		response="$(brew outdated --quiet | grep wget)";
		if [ "$response" == "wget" ]; then
			brew upgrade wget
		fi

		#Install/update python
		response="$(brew outdated --quiet | grep python)";
		if [ "$response" == "python" ]; then
			brew upgrade python
		fi
		
		brew cleanup --prune=30
		pip install --upgrade glances
	fi

	#Adding Vim Colors
	if [ "$VIMCOLORS" == "yes" ]; then
		  sayText "adding the vim colors"
		  sudo apt-get -y install vim
		  get_file vim.tar.bz2 https://bitbucket.org/agghorta/bash-profile/raw/$branch/vim.tar.bz2
		  tar -xvjf ${WorkPath}vim.tar.bz2 -C ~
		  rm ${WorkPath}vim.tar.bz2
		fi

	#Adding Sreenfetch
	if [ "$SCREENFETCH" == "yes" ]; then
		sayText "Adding ScreenFetch to the bash profile"
		get_file screenfetch 'https://raw.githubusercontent.com/KittyKatt/screenFetch/master/screenfetch-dev'
		sudo chmod +x ${WorkPath}screenfetch
		sudo mv ${WorkPath}screenfetch /usr/local/bin/
		echo "#---------------------" >> ${WorkPath}bash_profile_temp
		echo "# Calling ScreenFetch" >> ${WorkPath}bash_profile_temp
		echo "#---------------------" >> ${WorkPath}bash_profile_temp
		echo screenfetch -E >> ${WorkPath}bash_profile_temp
	fi

fi

if [ "$OS" == "mac" ]; then
	#adding image support to the terminal
	sayText "Adding image support"
	get_file imgls 'https://raw.githubusercontent.com/gnachman/iTerm2/master/tests/imgls'
	sudo chmod +x ${WorkPath}imgls
	sudo mv ${WorkPath}imgls /usr/local/bin/
	get_file imgcat 'https://raw.githubusercontent.com/gnachman/iTerm2/master/tests/imgcat'
	sudo chmod +x ${WorkPath}imgcat
	sudo mv ${WorkPath}imgcat /usr/local/bin/
fi

if [ "$OS" == "linux" ]; then

	#Set color database for ubuntu
	get_file dircolors 'https://bitbucket.org/agghorta/bash-profile/raw/$branch/colors/dir_colors.sh' 
	eval $(dircolors ${WorkPath}dircolors)

	sayText "Adding bash profile call to bashrc"
	if [ -f ~/.bashrc ]; then
		cp ~/.bashrc ${WorkPath}bashrc;
		sed -i '/# Andre bash profile start/,/# Andre bash profile end/d' ${WorkPath}bashrc;
	else
		cp /etc/skel/.bashrc ${WorkPath}bashrc;
	fi
	
	if [ -f ${WorkPath}bashrc ]; then
		echo "# Andre bash profile start" >> ${WorkPath}bashrc
		echo " " >> ${WorkPath}bashrc		
		echo " " >> ${WorkPath}bashrc
		echo "#---------------------" >> ${WorkPath}bashrc
		echo "# Calling bash_profile" >> ${WorkPath}bashrc
		echo "#---------------------" >> ${WorkPath}bashrc
		echo " " >> ${WorkPath}bashrc
	    echo "if [ -f ~/.bash_profile ]; then" >> ${WorkPath}bashrc
	    echo "   . ~/.bash_profile" >> ${WorkPath}bashrc
		echo "fi" >> ${WorkPath}bashrc
		echo " " >> ${WorkPath}bashrc
		echo " " >> ${WorkPath}bashrc
		echo "# Andre bash profile end" >> ${WorkPath}bashrc
		
	fi
fi

#adding the super user configuration
# if [ "$superuser" != "yes" ]; then
# 	sayText "Adding Super User Bash Profile";
# 	if [ "$OS" == "linux" ]; then
# 		sudo su -c "wget -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/$branch/install/install.sh" && sudo su -c "source ~/install.sh yes ${WorkPath}bash_settings" && sudo su -c "rm ~/install.sh"
# 	else
# 		sudo -u root -i eval "wget -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/$branch/install/install.sh" &&  sudo -u root -i eval "source ~/install.sh yes ${WorkPath}bash_settings" &&  sudo -u root -i eval "rm ~/install.sh"
# 	fi
# fi

# Moving bash files to the correct place
mv ${WorkPath}bash_profile_temp ~/.zshrc;
if [ "$OS" == "linux" ]; then
	mv ${WorkPath}bashrc ~/.bashrc;
fi

#Removing unecessary files
sayText "Cleaning up"
rm -rf ${WorkPath};

sayText "Firing up"
# source ~/.bash_profile;

