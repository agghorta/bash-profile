#------------------
# Custom Functions
#------------------

# ff:  to find a file under the current directory
ff () { /usr/bin/find . -name "$@"; }
# ffs: to find a file whose name starts with a given string
ffs () { /usr/bin/find . -name "$@"'*'; }
# ffe: to find a file whose name ends with a given string
ffe () { /usr/bin/find . -name '*'"$@"; }

findlarger() { find . -type f -size +${1}c; }

# Create a new directory and enter it
function md() {
        mkdir -p "$@" && cd "$@"
}

# Custom grep wrapper
g() {
    grep -HRins -C3 --colour=always "$@" .;
}

# Display a text with borders to console 
function sayText() {
		echo ""
		echo ""
        echo "****************************"
        echo "*"
        echo "* $1"
        echo "*"
        echo "****************************"
		echo ""
}

# Search and destroy, searches text in file, then replaces first arg with second arg
sad() { grep -rls $1 . | grep -v svn | xargs sed -i -e "s/$1/$2/"; }

profileversion() {
  echo " 
    #BASH_PROFILE_VERSION

    "
}

updatebashprofile() {
  force=$1;
  pwd=$(pwd);
  cd ~;
  if [ "$force" == "force" ]; then
    echo "updating bash profile";
    wget --no-cache -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/prod/install/install.sh && source ~/install.sh && rm ~/install.sh;
  else
    wget --no-cache --quiet -O ~/bversion.txt "https://bitbucket.org/agghorta/bash-profile/raw/prod/version.txt";
    newVersion=$(<bversion.txt);
    currentVersion="$(profileversion | xargs)";
    if [ "$newVersion" == "$currentVersion" ]; then
      echo "bash profile is up to date";
      rm bversion.txt;
    else
      echo "updating bash profile";
      rm bversion.txt;
      wget --no-cache -O ~/install.sh https://bitbucket.org/agghorta/bash-profile/raw/prod/install/install.sh && source ~/install.sh && rm ~/install.sh;
    fi
  fi
  cd $pwd;
}

# Use Git’s colored diff when available
hash git &>/dev/null
if [ $? -eq 0 ]; then
        function diff() {
                git diff --no-index --color-words "$@"
        }
fi

#run php lint before git add
function git_add() { php -l $1 && git add $1; }

function gitCheckAdd() {
  file=$1;
  git diff $1;
  echo "Add to Git?"
  select addToGit in "yes" "no"; do
    if [ "$addToGit" == "no" ]; then
      break;
    else
      git_add $1;
      break;
    fi
  done
}

#cleanup local git branchs
function cleanupbranches() {
  git checkout master
  git fetch -p
  git remote prune origin
  git branch --merged master | grep -v 'master$' | xargs -n 1 git branch -d
}

#automate git rebase to do a git fetch
function git_rebase() { git fetch && git rebase -i origin/master; }

#print the current git branch
function print_git_branch() { git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* //'; }

#print the left square brace if there is a git branch
function sb1() { 
  A=`print_git_branch`
  B="" 
  if [ "$A" == "$B" ] 
  then
    echo "";
  else
    echo "[";
  fi
}

#print the right square brace if there is a git branch
function sb2() {  
  A=`print_git_branch`
  B=""
  if [ "$A" == "$B" ] 
  then
    echo "";
  else
    echo "]";
  fi
}