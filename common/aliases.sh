#----------
# Aliases:
#----------
alias myip="ifconfig"
alias help="man"
alias mysqladmin="/usr/local/mysql/bin/mysqladmin"
alias grep="grep -Hins -C3 --colour=always"
alias fgrep='fgrep -Hins -C3 --color=always'
alias egrep='egrep -Hins -C3 --color=always'
alias l.='ls -d .* --color=auto'
alias findtext="grep -r -o -i $1 *"
alias please='sudo $(fc -ln -1)'
alias bashversion="profileversion"
alias bashupdate="updatebashprofile"
alias bashprofileupdate="updatebashprofile"
alias dirsize="du -chd 0 *"
alias foldersize="du -chd 0 *"


# git aliases
alias gd="git difftool -y --tool=opendiff "
alias gmt="git mergetool -y --tool=opendiff "
alias s="git status "
alias b="git branch "
alias gcom="git commit -am "
alias gc="git commit -m "
alias merge="git merge "
alias stash="git stash "
alias p=pull
alias push="git push "
alias gprm="git pull -r origin master"
alias gpr="git pull -r origin "
alias ga=git_add
alias gca=gitCheckAdd
alias reb=git_rebase
alias c="git checkout "
alias glog="git log --decorate --pretty=short --graph --source "