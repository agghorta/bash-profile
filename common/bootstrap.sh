#--------------
# Startup
#--------------

export EDITOR="vi"

if [[ ! -x $(which fink) && -d /sw/bin ]];then
        source /sw/bin/init.sh
fi

if [ -f ~/.git-completion.bash ]; then
    . ~/.git-completion.bash
fi

export PATH=/usr/local/bin:/opt/local/bin:/opt/local/sbin:$PATH

HISTCONTROL=ignoreboth
HISTSIZE=1500 # size of history file