
# #General Settings
SERVER_NAME=$( hostname )" - Super User"

# #What apps to install
SCREENFETCH="no"

#Color settings
BASH_COLOR='RED'
SERVER_NAME_COLOR='LIGHT_RED'
INFO_TEXT_COLOR='LIGHT_WHITE'
GIT_COLOR='LIGHT_GREEN'