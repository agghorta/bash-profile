#General Settings
OS="linux"
ENVIRONMENT_INCLUDES=( "common" "home" )
SERVER_NAME="Elastic 2"

#What apps to install
SCREENFETCH="yes"
VIMCOLORS="yes"

#Color settings
BASH_COLOR='BLUE'
SERVER_NAME_COLOR='LIGHT_BLUE'
INFO_TEXT_COLOR='LIGHT_WHITE'
GIT_COLOR='LIGHT_GREEN'