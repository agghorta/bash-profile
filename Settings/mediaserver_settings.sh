#General Settings
OS="linux"
ENVIRONMENT_INCLUDES=( "common" "home" )
SERVER_NAME="MediaServer"

#What apps to install
SCREENFETCH="yes"
VIMCOLORS="yes"

#Color settings
BASH_COLOR='LIGHT_GREEN'
SERVER_NAME_COLOR='GREEN'
INFO_TEXT_COLOR='YELLOW'
GIT_COLOR='LIGHT_GREEN'