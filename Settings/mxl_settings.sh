#General Settings
OS="linux"
ENVIRONMENT_INCLUDES=( "common" "home" )
SERVER_NAME="mxlengineering"

#What apps to install
SCREENFETCH="no"
VIMCOLORS="yes"

#Color settings
BASH_COLOR='MXL_GREEN'
SERVER_NAME_COLOR='MXL_YELLOW'
INFO_TEXT_COLOR='LIGHT_WHITE'
GIT_COLOR='LIGHT_GREEN'