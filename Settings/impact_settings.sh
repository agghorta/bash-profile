#General Settings
OS="linux"
ENVIRONMENT_INCLUDES=( "common" "home" )
SERVER_NAME="ImpactDrywall"

#What apps to install
SCREENFETCH="no"
VIMCOLORS="yes"

#Color settings
BASH_COLOR='DARK_ORANGE'
SERVER_NAME_COLOR='ORANGE'
INFO_TEXT_COLOR='LIGHT_WHITE'
GIT_COLOR='LIGHT_GREEN'