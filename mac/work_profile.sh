#Work MAC Specific profile settings

#----------
# zsh sets
#----------

setopt AUTO_CD
# share history across multiple zsh sessions
setopt SHARE_HISTORY
# append to history
setopt APPEND_HISTORY
# adds commands as they are typed, not at shell exit
setopt INC_APPEND_HISTORY
# expire duplicates first
setopt HIST_EXPIRE_DUPS_FIRST 
# do not store duplications
setopt HIST_IGNORE_DUPS
#ignore duplicates when searching
setopt HIST_FIND_NO_DUPS
# removes blank lines from history
setopt HIST_REDUCE_BLANKS
#autocorrect
setopt CORRECT
setopt CORRECT_ALL

#----------
# Exports and ETC
#----------
# Configuration for Activator for scala Dev
export PROJECTS_HOME="/Users/ahorta/projects"
export ACTIVATOR_HOME=$PROJECTS_HOME/activator
export PATH=$PATH:$ACTIVATOR_HOME/bin:/usr/local/bin/pipenv

