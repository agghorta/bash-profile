#Common MAC OS bash profile settings

#----------
# Functions:
#----------

# git pull
function pull() {
    pwd=$(pwd);
    if [ "$1" == "all" ]
    then
        base_folders=$(ls -d1 $HOME/projects/*);
        for base_folder in $base_folders; do
           projects=$(ls -d1 $base_folder/*/);
           for project in $projects; do
              sayText "Getting $(basename $project)";
              echo $project
              git -C $project pull;
           done
        done
        cd "$pwd";
    else
        git pull $1 $2 $3 $4;
    fi    
    
}

# Remove the Mac OS extended Attributes
function removeExtendedAttributes() {
    sudo chflags -R nouchg $1; 
    xattr -r -c $1;
}

#----------
# Exports and ETC
#----------

# Pip is using virtualenv. That is forcing pip to use it.
export PIP_REQUIRE_VIRTUALENV=true
# For pip, On some systems you may need to export a PYTHONPATH variable depending on the tool you're using.
export PYTHONPATH=$HOME/local/lib/python2.7/site-packages/
# Authentication Key for infomart NPM
export NEXUS_NPM_AUTH=YWRyaWFuOkBkcmlhbmNvZGVzbGFiczEyM3E=

# Set AWS Keys to ssh to aws and work with aws command line
export AWS_ACCESS_KEY="AKIAJWHV3XBIWKEHXT4Q"
export AWS_SECRET_KEY="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"
export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY
export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi

#----------
# Aliases:
#----------
alias ls='ls -GFh'
alias ll="ls -alhF"
alias la="ls -A"
alias l='ls -CF'
alias sudo='sudo '
alias su='su -'
alias unlockFolder="removeExtendedAttributes"

#-----------------
# Terminal Colors
#-----------------
export CLICOLOR=1
export LSCOLORS=exfxcxdxbxegedabagacad