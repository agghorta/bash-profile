#-----------------------
# Custom Command Prompt
#-----------------------
NO_COLOR="\[\e[m\]"
LIGHT_WHITE="\[\033[1;37m\]"
WHITE="\[\033[0;37m\]"
GRAY="\[\e[1;30m\]"
BLACK="\[\033[0;30m\]"
RED="\[\033[0;31m\]"
LIGHT_RED="\[\033[1;31m\]"
GREEN="\[\033[0;32m\]"
LIGHT_GREEN="\[\033[1;32m\]"
YELLOW="\[\033[0;33m\]"
LIGHT_YELLOW="\[\033[1;33m\]"
BLUE="\[\033[0;34m\]"
LIGHT_BLUE="\[\033[1;34m\]"
MAGENTA="\[\033[0;35m\]"
LIGHT_MAGENTA="\[\033[1;35m\]"
CYAN="\[\033[0;36m\]"
LIGHT_CYAN="\[\033[1;36m\]"

# Para adicionar cores da paleta de 256 cores usar a funcao abaixo com o numero da cor desejada. Ver Orange. Como referencia para cores ver https://misc.flogisoft.com/bash/tip_colors_and_formatting
function EXT_COLOR () { echo -ne "\[\033[38;5;$1m\]"; }
ORANGE=`EXT_COLOR 172`
DARK_ORANGE=`EXT_COLOR 208`
MXL_GREEN=`EXT_COLOR 41`
MXL_YELLOW=`EXT_COLOR 226`

COLOR_SETTINGS_PLACE_HOLDER


PS1="${BASH_COLOR}┌─${SERVER_NAME_COLOR}${SERVER_NAME}${BASH_COLOR}───[${INFO_TEXT_COLOR}\t${NO_COLOR}${BASH_COLOR}][${INFO_TEXT_COLOR}\w${NO_COLOR}${BASH_COLOR}]\$(sb1)${NO_COLOR}${GIT_COLOR}\$(print_git_branch)${NO_COLOR}${BASH_COLOR}\$(sb2)[${NO_COLOR}${INFO_TEXT_COLOR}!\!${NO_COLOR}${BASH_COLOR}]${NO_COLOR} 
${BASH_COLOR}└──▪${NO_COLOR} "

export GREP_COLOR='1;33'
