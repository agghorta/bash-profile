#-----------------------
# Custom Command Prompt
#-----------------------
WHITE="white"
LIGHT_WHITE="255"
BLACK="black"
GRAY="8"
DARK_GRAY="240"
SILVER="7"
RED="196"
LIGHT_RED="red"
GREEN="46"
LIGHT_GREEN="green"
YELLOW="226"
LIGHT_YELLOW="yellow"
BLUE="69"
LIGHT_BLUE="blue"
MAGENTA="164"
LIGHT_MAGENTA="magenta"
CYAN="159"
LIGHT_CYAN="cyan"
ORANGE="166"

#git
RPROMPT_PREFIX='%{'$'\e[1A''%}' # one line up
RPROMPT_SUFFIX='%{'$'\e[1B''%}' # one line down

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT="$RPROMPT_PREFIX"\$vcs_info_msg_0_"$RPROMPT_SUFFIX"   #hack para ter o rprompt na primeira linha
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' formats '%F{$LIGHT_GREEN}<%b>%F{$ORANGE}%u%c%f'
zstyle ':vcs_info:git:*' stagedstr ' (*)'
zstyle ':vcs_info:git:*' unstagedstr ' (!)'


COLOR_SETTINGS_PLACE_HOLDER


PROMPT="%F{$BASH_COLOR}┌─%F{$SERVER_NAME_COLOR}${SERVER_NAME}%F{$BASH_COLOR}───[%F{$INFO_TEXT_COLOR}%*%F{$BASH_COLOR}][%B%F{$INFO_TEXT_COLOR}%~%b%F{$BASH_COLOR}][%F{$INFO_TEXT_COLOR}%h%F{$BASH_COLOR}][%(?.%F{green}✓.%F{red}✗)%F{$BASH_COLOR}]%f
%F{$BASH_COLOR}└──▪%f "

export GREP_COLOR='1;33'

